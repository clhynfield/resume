GULP := $(shell command -v gulp 2> /dev/null)

all : dependencies
ifndef GULP
	yarn global add gulp-cli
endif
	gulp

dependencies : node_modules

node_modules :
	yarn install


clean :
	rm -rf node_modules .tmp public yarn-error.log yarn.lock

test :
	gulp test

