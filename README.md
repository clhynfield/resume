# Résumé

A web résumé you can easily publish to GitHub Pages

## Quick Start

  1. Open `app/index.html` and fill in your own personal
     information, work experience, education, and skills.
  2. Open `app/styles/personal.less` and tweak the colors
     and fonts to suit your personal style.
  3. Preview and publish the content in `app`. More on that
     below.

## Development

### Develop static content locally

I developed most of the static content and styling on an iPad Pro,
and it remains a goal of this project to enable quick and easy
content development on simple, post-PC devices.

You'll find all the content and styling in `app`, and it should
render all client-side just fine from there.

To upgrade the vendored Less package:

```shell
curl -o app/vendor/less/less-3.13.1.min.js https://cdn.jsdelivr.net/npm/less@3.13.1
```

### Develop and build with Gulp

For a full developer workflow, including live-reloading preview,
and stylesheet preprocessing and optimization, you'll need to
install [Node][] and [Gulp][].

```shell
gulp develop
```

### Updating fonts

```shell
cd app/fonts

curl -L -O https://github.com/CatharsisFonts/Cormorant/releases/download/v3.609/Cormorant_Webfonts_v3.609.zip
unzip Cormorant_Webfonts_v3.609.zip
mv Cormorant_Webfonts_v3.609/3.\ Webfont\ Files/*.woff* ./
rm -rf Cormorant_Webfonts_v3.609*

curl -L -o aleo-1.3.1.zip https://github.com/AlessioLaiso/aleo/archive/1.3.1.zip 
unzip aleo-1.3.1.zip
mv aleo-1.3.1/fonts/woff*/*.woff* ./
rm -rf aleo-1.3.1*

curl -L -o Lato2OFLWeb.zip https://www.latofonts.com/download/lato2oflweb-zip/
unzip Lato2OFLWeb.zip 
mv Lato2OFLWeb/LatoLatin/fonts/*.woff* ./
rm -rf __MACOSX Lato2OFLWeb*
```

## Publishing

### With GitHub Pages

## Colophon

I've shared some details on my own personal typeface choices at [!11]. 

- [Cormorant][]
- [Aleo][]
- [Lato][]

[Node]: https://nodejs.org
[Gulp]: http://gulpjs.com

[Cormorant]: https://github.com/CatharsisFonts/Cormorant (GitHub - CatharsisFonts/Cormorant: Cormorant open-source display font family)
[Lato]: https://github.com/latofonts/lato-source/ (GitHub - latofonts/lato-source: Source files for the Lato fonts)
[Aleo]: https://github.com/AlessioLaiso/aleo (GitHub - AlessioLaiso/aleo: Aleo font family)

