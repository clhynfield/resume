const { src, dest, watch, series } = require('gulp')
const $ = require('gulp-load-plugins')()
const browserSync = require('browser-sync').create()
const reload = browserSync.reload

var dev = false

process.env.OPENSSL_CONF = '/etc/ssl'

function lintHtml(files) {
  return src(files)
    .pipe($.htmllint({ rules: { 'indent-width': 2 } }))
    .pipe(reload({ stream: true, once: true }))
}

function lint() {
  return lintHtml('app/**/*.html')
    .pipe(dest('app'))
}

function assembleStyles() {
  return src(['app/styles/*.css', 'app/styles/*.less'])
    .pipe($.if(/\.less$/, $.less()))
    .pipe($.if(dev, $.sourcemaps.init()))
    .pipe($.autoprefixer())
    .pipe($.if(dev, $.sourcemaps.write()))
    .pipe(dest('.tmp/styles'))
    .pipe(reload({ stream: true }))
}

function assembleHtml() {
  return src('app/*.html')
    .pipe($.useref({ searchPath: ['.tmp', 'app', '.'] }))
    .pipe($.if(/\.js$/, $.uglify({ compress: { drop_console: true } })))
    .pipe($.if(/\.css$/, $.less()))
    .pipe($.if(/\.css$/, $.cssnano({ safe: true, autoprefixer: false })))
    .pipe($.if(/\.html$/, $.template({ date: new Date().toISOString() })))
    .pipe($.if(/\.html$/, $.htmlmin({
      collapseWhitespace: true,
      minifyCSS: true,
      minifyJS: { compress: { drop_console: true } },
      processConditionalComments: true,
      removeComments: true,
      removeEmptyAttributes: true,
      removeScriptTypeAttributes: true,
      removeStyleLinkTypeAttributes: true
    })))
    .pipe(dest('public'))
}

function assembleFonts() {
  return src('app/fonts/**/*')
    .pipe($.if(dev, dest('.tmp/fonts'), dest('public/fonts')))
}

function assembleExtras() {
  return src([
    'app/*',
    '!app/*.html'
  ], {
    dot: true
  }).pipe(dest('public'))
}

function assemblePdf() {
  const base = 'file://' + process.cwd() + '/public/'
  return src('public/index.html')
    .pipe($.htmlPdf({
      format: 'letter',
      orientation: 'portrait',
      border: {
        top: '1in',
        bottom: '1in',
        right: '0.5in',
        left: '0.5in'
      },
      base: base
    }))
    .pipe($.rename('ClaytonHynfield.pdf'))
    .pipe(dest('public'))
}

function build() {
  return src('public/**/*').pipe($.size({ title: 'build', gzip: true }))
}

exports.default = series(
  lint,
  assembleFonts,
  assembleExtras,
  assembleHtml,
  assemblePdf,
  build
)

function serveDev(done) {
  browserSync.init({
    notify: false,
    port: 9000,
    server: {
      baseDir: ['.tmp', 'app', 'public']
    }
  })

  watch([
    'app/*.html',
    'app/styles/**/*',
    '.tmp/fonts/**/*'
  ]).on('change', reload)

  watch(['app/styles/**/*.css', 'app/styles/**/*.less'], series(assembleStyles))
  watch('app/fonts/**/*', series(assembleFonts))
  done()
}

exports.develop = series(assembleStyles, assembleFonts, serveDev)
